#include <string.h>
#include <iostream>

#include "benchmark.h"

void BM_false_branch(benchmark::State &state)
{
    srand(1);
    const unsigned int N = state.range(0);
    std::vector<unsigned long> v1(N), v2(N);
    std::vector<int> c1(N), c2(N);
    for (size_t i = 0; i < N; ++i)
    {
        v1[i] = rand();
        v2[i] = rand();
        c1[i] = rand() & 0x1;
        c2[i] = !c1[i];
    }

    for (auto _ : state)
    {
        unsigned long a1 = 0, a2 = 0;

        for (size_t i = 0; i < N; ++i)
        {
            if (bool(c1[i]) + bool(c2[i]))
            {
                a1 += v1[i];
            }
            else
            {
                a1 *= v2[i];
            }
        }
        benchmark::DoNotOptimize(a1);
        benchmark::DoNotOptimize(a2);
    }
    state.SetItemsProcessed(N * state.iterations());
}
BENCHMARK(BM_false_branch)->Arg(1 << 22);
BENCHMARK_MAIN();