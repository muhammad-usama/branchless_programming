#include <iostream>
#include <stdlib.h>

#include "benchmark.h"

void BM_branch_predicted(benchmark::State &state)
{
    srand(1);
    const unsigned int N = state.range(0);
    std::vector<unsigned long> v1(N), v2(N);
    std::vector<int> c1(N);
    for (size_t index = 0; index < N; ++index)
    {
        v1[index] = rand();
        v2[index] = rand();
        c1[index] = rand() >= 0;
    }
    for (auto _ : state)
    {
        unsigned long a1 = 0, a2 = 0;
        for (size_t index = 0; index < N; ++index)
        {
            if (c1[index])
                a1 += v1[index];
            else
                a2 += v2[index];
        }
        benchmark::DoNotOptimize(a1);
        benchmark::DoNotOptimize(a2);
    }
    state.SetItemsProcessed(N * state.iterations());
}
BENCHMARK(BM_branch_predicted)->Arg(1 << 22);
BENCHMARK_MAIN();