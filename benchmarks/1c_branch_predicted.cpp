#include <iostream>
#include <stdlib.h>

#include "benchmark.h"

void BM_branch_predicted(benchmark::State &state)
{
    srand(1);
    const unsigned int N = state.range(0);
    std::vector<unsigned long> v1(N), v2(N);
    std::vector<int> c1(N);
    v1[0] = rand();
    v2[0] = rand();
    c1[0] = rand() & 0x1;
    for (size_t i = 1; i < N; ++i)
    {
        v1[i] = rand();
        v2[i] = rand();
        c1[i] = !c1[i - 1];
    }
    for (auto _ : state)
    {
        unsigned long a1 = 0, a2 = 0;
        for (size_t i = 0; i < N; ++i)
        {
            if (c1[i])
                a1 += v1[i];
            else
                a2 += v2[i];
        }
        benchmark::DoNotOptimize(a1);
        benchmark::DoNotOptimize(a2);
    }
    state.SetItemsProcessed(N * state.iterations());
}
BENCHMARK(BM_branch_predicted)->Arg(1 << 22);
BENCHMARK_MAIN();