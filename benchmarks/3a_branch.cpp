#include <string.h>
#include <iostream>

#include "benchmark.h"

void BM_branched(benchmark::State &state)
{
    srand(1);
    const unsigned int N = state.range(0);
    std::vector<unsigned long> v1(N), v2(N);
    std::vector<int> c1(N);
    for (size_t i = 0; i < N; ++i)
    {
        v1[i] = rand();
        v2[i] = rand();
        c1[i] = rand() & 0x1;
    }

    for (auto _ : state)
    {
        unsigned long a1 = 0, a2 = 0;

        for (size_t i = 0; i < N; ++i)
        {
            if (c1[i])
            {
                a1 += v1[i] - v2[i];
            }
            else
            {
                a2 += v1[i] * v2[i];
            }
        }
        benchmark::DoNotOptimize(a1);
        benchmark::DoNotOptimize(a2);
    }
    state.SetItemsProcessed(N * state.iterations());
}
BENCHMARK(BM_branched)->Arg(1 << 22);
BENCHMARK_MAIN();