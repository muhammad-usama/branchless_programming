#include <string.h>
#include <iostream>

#include "benchmark.h"

void __attribute__((noinline)) f(bool b, unsigned long x, unsigned long &s)
{
    s += b * x;
}

void BM_5_branchless(benchmark::State &state)
{
    srand(1);
    const unsigned int N = state.range(0);
    std::vector<unsigned long> v1(N), v2(N);
    std::vector<int> c1(N);
    for (size_t i = 0; i < N; ++i)
    {
        v1[i] = rand();
        c1[i] = rand() & 0x1;
    }

    for (auto _ : state)
    {
        unsigned long a1 = 0;
        for (size_t i = 0; i < N; ++i)
        {
            f(c1[i], v1[i], a1);
        }
        benchmark::DoNotOptimize(a1);
    }
    state.SetItemsProcessed(N * state.iterations());
}

BENCHMARK(BM_5_branchless)->Arg(1 << 22);
BENCHMARK_MAIN();
